///**
// * 
// */
//package edu.nwmissouri.geoapp.test.service;
//
//import static org.junit.Assert.*;
//
//import java.util.List;
//
//import javax.transaction.Transactional;
//
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import edu.nwmissouri.geoapp.model.TblPool;
//import edu.nwmissouri.geoapp.serviceImpl.PoolServiceImpl;
//import edu.nwmissouri.geoapp.test.GeoAppUnitTesting;
//
///**
// * @author S522572
// *
// */
//@Transactional
//public class PoolServiceTest extends GeoAppUnitTesting{
//	
//	@Autowired 
//	PoolServiceImpl poolServiceImpl;
//
//
//	/**
//	 * @throws java.lang.Exception
//	 */
//	@BeforeClass
//	public static void setUpBeforeClass() throws Exception {
//	}
//
//	/**
//	 * @throws java.lang.Exception
//	 */
//	@AfterClass
//	public static void tearDownAfterClass() throws Exception {
//	}
//
//	/**
//	 * @throws java.lang.Exception
//	 */
//	@Before
//	public void setUp() throws Exception {
//	}
//
//	/**
//	 * @throws java.lang.Exception
//	 */
//	@After
//	public void tearDown() throws Exception {
//		// clean up after each test method
//	}
//
//	@Test
//	public final void testFindAll() {
//		
//		List<TblPool> tblPools = poolServiceImpl.getPools();
//		Assert.assertNotNull("failure - expected not null",tblPools );
//		Assert.assertEquals("failure - expected list size", 4, tblPools.size() );
//		
//	}
//       
//	@Test
//	public final void testFindOne() {
//		
//		int assignID = 400004;
//		TblPool tblPool = poolServiceImpl.findTblPoolByassignID(assignID);
////		Assert.assertNotNull("failure - expected not null",tblPool );
//		Assert.assertEquals(assignID, tblPool.getTblAssignment().getAssignID() );
//		
//	}
//	
//}
