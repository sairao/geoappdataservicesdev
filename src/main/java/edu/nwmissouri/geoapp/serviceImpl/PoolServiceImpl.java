package edu.nwmissouri.geoapp.serviceImpl;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.qos.logback.core.net.SyslogOutputStream;
import edu.nwmissouri.geoapp.model.TblAssignment;
import edu.nwmissouri.geoapp.model.TblPool;
import edu.nwmissouri.geoapp.model.TblPoolquestion;
import edu.nwmissouri.geoapp.model.TblPoolquestionoption;
import edu.nwmissouri.geoapp.model.TblQuiz;
import edu.nwmissouri.geoapp.model.TblStudent;
import edu.nwmissouri.geoapp.model.TblStudentquiz;
import edu.nwmissouri.geoapp.model.TblStudentquiztake;
import edu.nwmissouri.geoapp.repository.AssignmentRepository;
import edu.nwmissouri.geoapp.repository.PoolRepository;
import edu.nwmissouri.geoapp.repository.PoolquestionRepository;
import edu.nwmissouri.geoapp.repository.PoolquestionoptionRepository;
import edu.nwmissouri.geoapp.repository.QuizRepository;
import edu.nwmissouri.geoapp.repository.StudentRepository;
import edu.nwmissouri.geoapp.repository.StudentquizRepository;
import edu.nwmissouri.geoapp.repository.StudentquiztakeRepository;
import edu.nwmissouri.geoapp.service.PoolService;


@Service
public class PoolServiceImpl implements PoolService {
	
	@Autowired
	PoolquestionRepository poolQuestionRepository;	
	
	@Autowired
	PoolquestionoptionRepository poolQuestionOptionRepository;	
	
    @Autowired
	QuizRepository quizRepository; 
    
    @Autowired
	AssignmentRepository assignmentRepository;	
    
    @Autowired
    StudentRepository studentRepository;	
    
    @Autowired
    StudentquizRepository studentquizRepository;	
    
    @Autowired
    StudentquiztakeRepository studentquiztakeRepository;	
    
    @Autowired
    PoolRepository poolRepository;
    
   

	@Override 
	public List<String> getPoolQuestions(int poolID) {
		// TODO Auto-generated method stub
		return poolQuestionRepository.getPoolQuestions(poolID);
	}

	@Override 
	public List<TblPoolquestionoption> getQuestionChoices( ) {
		// TODO Auto-generated method stub
		return poolQuestionOptionRepository.findAll(); 
	}

    @Override 
    public String getCountofQuestions(int quizID) {
    	
    	return quizRepository.getCountofQuestions(quizID);
    }

	@Override
	public void saveScore(int score, int studentID, int assignID) throws ParseException {
		
		TblAssignment tblassignment = assignmentRepository.findTblAssignmentByassignID(assignID); 
		
		TblStudent tblstudent = studentRepository.findTblStudentBystudentID(studentID) ; 
		
		
		int takeNum = (int) studentquiztakeRepository.getCountofNumTakes(studentID, tblassignment.getTblQuiz().getQuizID());
		
		TblStudentquiztake tblstudentquiztake = new TblStudentquiztake( takeNum+1,  score,  tblassignment.getTblQuiz(),  tblstudent);
		
		TblStudentquiz tblStudentquiz = studentquizRepository.findTblStudentquizbystuIDnquizID(studentID, tblassignment.getTblQuiz().getQuizID());
		
		TblStudentquiztake tblstudentquiztake2 = studentquiztakeRepository.save(tblstudentquiztake);
		
		
		
		TblStudentquiztake tblstudentquiztake3 = studentquiztakeRepository.findTblStudentquiztakeBystudentQuizTakeID(tblstudentquiztake2.getStudentQuizTakeID());
		
		float q = tblassignment.getTblQuiz().getNumQuestions();
		 float s = score;
		 int minpercent = tblassignment.getTblQuiz().getQualpercent(); 
		 int markspercent = (int) ((s/q) *100); 
		 int noofgivenattempts = tblassignment.getTblQuiz().getNum_Takes_Max();  		 
		 // finding out whether the user has failed or passed the test
		 String passorfail = "";
		 if ( markspercent >= minpercent) {
			 passorfail = "pass";
			 
		 }else 
		 {
			 passorfail = "fail";
		 }
		
		 

		if (tblStudentquiz == null) {
			
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String myDate = fmt.format(new Date());
			TblStudentquiz tblStudentquiz1 = new TblStudentquiz( 1,  score, passorfail ,tblassignment.getTblQuiz(),  tblstudent);
			tblStudentquiz1.setLastUpdatedTime(myDate);
			studentquizRepository.save(tblStudentquiz1);
		}
		else {
			
			int maxScore = Math.max(score, tblStudentquiz.getMaxScore() );
			tblStudentquiz.setMaxScore(maxScore);			
			tblStudentquiz.setNumTakes(tblStudentquiz.getNumTakes()+1);
			System.out.println(tblStudentquiz.getGrade());
			if (tblStudentquiz.getGrade().equals("fail")) {
				
				tblStudentquiz.setGrade(passorfail);
			}
			
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String myDate = fmt.format(new Date());
			
			tblStudentquiz.setLastUpdatedTime(myDate);
			studentquizRepository.save(tblStudentquiz);
		}
		
		
	}
	
	@Override
	public String checkAttemptsBeforeTakeQuiz(Integer assignID, Integer studentID) throws ParseException {

		TblAssignment tblassignment = assignmentRepository.findTblAssignmentByassignID(assignID); 
		
		TblStudent tblstudent = studentRepository.findTblStudentBystudentID(studentID) ;
				
		TblStudentquiz tblStudentquiz = studentquizRepository.findTblStudentquizbystuIDnquizID(studentID, tblassignment.getTblQuiz().getQuizID());
		
		int num_takes_max = quizRepository.findTblQuiztByquizID(tblassignment.getTblQuiz().getQuizID()).getNum_Takes_Max(); 
		
		if (tblStudentquiz == null) {
			
			/*TblStudentquiz tblStudentquiz1 = new TblStudentquiz( 1,  score,  tblassignment.getTblQuiz(),  tblstudent);			
			studentquizRepository.save(tblStudentquiz1);*/
			
			return "Take test";
		}
		else {
			
			int takeCount = tblStudentquiz.getNumTakes();
			
			if(takeCount == num_takes_max){
				
				return "Please see the instructor to get access to your assignment" ; 
			}else{
				
				SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				String myDate = fmt.format(new Date());
				SimpleDateFormat fmt2 = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
				Date currentDate = fmt2.parse(myDate);
							
				String lastUpdatedDate = tblStudentquiz.getLastUpdatedTime();
				Date lastUpdated = fmt2.parse(lastUpdatedDate);
				
				double lockOutHrs	=	tblassignment.getTblQuiz().getLockoutHrs();		
				double lockOutMilliSecs = lockOutHrs * 3600 * 1000 ;
				
				double waitingHrs =  (double)(currentDate.getTime() - lastUpdated.getTime())/(3600*1000) ;
				long diff = currentDate.getTime() - lastUpdated.getTime();
				
				if((currentDate.getTime() - lastUpdated.getTime()) < 0){
					waitingHrs =  (double)(lastUpdated.getTime() - currentDate.getTime() )/(3600*1000) ;
					diff = lastUpdated.getTime() - currentDate.getTime();   
				}
				
				
				DecimalFormat df=new DecimalFormat("0.00");
				String formate = df.format((lockOutHrs-waitingHrs)); 
				double finalValue = (Double) df.parse(formate) ;
								
					if((diff) >= lockOutMilliSecs){					
						return "Take test";					
					}
					else {					
						
						return "Please wait for " + finalValue + " hours to retake your test";
					}
				
				
				
			}
			
			}
		
		}
		

	public String checkAttempts(Integer assignID, Integer studentID) throws ParseException {
		
		
		TblAssignment tblassignment = assignmentRepository.findTblAssignmentByassignID(assignID); 
		
		int num_takes_max = quizRepository.findTblQuiztByquizID(tblassignment.getTblQuiz().getQuizID()).getNum_Takes_Max();
		
/*		TblStudentquiztake tblstudentquiztake = studentquiztakeRepository.findTblStudentquiztakeBymaxTakeNum(studentID, tblassignment.getTblQuiz().getQuizID(), num_takes_max);
*/

		TblStudentquiz tblStudentquiz = studentquizRepository.findTblStudentquizbystuIDnquizID(studentID, tblassignment.getTblQuiz().getQuizID());

		int takeCount = tblStudentquiz.getNumTakes();
		
					
			if(takeCount == num_takes_max){
				
				return "Please see the instructor to get access to your assignment" ; 
			}else{
				
				SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				String myDate = fmt.format(new Date());
				SimpleDateFormat fmt2 = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
				Date currentDate = fmt2.parse(myDate);
							
				String lastUpdatedDate = tblStudentquiz.getLastUpdatedTime();
				Date lastUpdated = fmt2.parse(lastUpdatedDate);
				
				double lockOutHrs	=	tblassignment.getTblQuiz().getLockoutHrs();		
				double lockOutMilliSecs = lockOutHrs * 3600 * 1000 ;
				
				double waitingHrs =  (double)(currentDate.getTime() - lastUpdated.getTime())/(3600*1000) ;
				long diff = currentDate.getTime() - lastUpdated.getTime();
				
				if((currentDate.getTime() - lastUpdated.getTime()) < 0){
					waitingHrs =  (double)(lastUpdated.getTime() - currentDate.getTime() )/(3600*1000) ;
					diff = lastUpdated.getTime() - currentDate.getTime();   
				}
				
				
				DecimalFormat df=new DecimalFormat("0.00");
				String formate = df.format((lockOutHrs-waitingHrs)); 
				double finalValue = (Double) df.parse(formate) ;
								
					if((diff) >= lockOutMilliSecs){					
						return "Take test";					
					}
					else {					
						
						return "Please wait for " + finalValue + " hours to retake your test";
					}
				
				
				
			}
			
			}
		
	

	
		
			
	public TblQuiz findTblQuizbyQuizID (int quizID) {
		
		return quizRepository.findTblQuiztByquizID(quizID);
		
		
		
	}

	
	@Override
/*	public void savePool(String[] arr, String poolName, String poolDesc) {
*/		public void savePool(String[] arr, String poolName, String loginName) {


/*		TblAssignment tblassignment = assignmentRepository.findTblAssignmentByassignID(400005); 
*/		
		
	/*	TblPool tblPool = new TblPool(poolName,poolDesc );*/
		TblPool tblPool = new TblPool(poolName, loginName);
		poolRepository.save(tblPool);
          int poolID = poolRepository.findPoolID(poolName, loginName);
		for(int i = 0; i < (arr.length/5)*5; i = i+5){
			
			TblPoolquestion tblPoolquestion = new TblPoolquestion(3, arr[i], poolRepository.findTblPooltBypoolID(poolID));
			poolQuestionRepository.save(tblPoolquestion);
			
			for(int j=i+1; j<i+5; j++){
				String answer = arr[j].toLowerCase();
				
				if(answer.contains("correct answer")){
					answer = answer.replace("correct answer", "");
					//String choice, int displayOrder, int fractionCorrect,TblPoolquestion tblPoolquestion
					TblPoolquestionoption tblPoolquestionoption = new TblPoolquestionoption(answer,5,1,tblPoolquestion);
					poolQuestionOptionRepository.save(tblPoolquestionoption);
				}else{
					TblPoolquestionoption tblPoolquestionoption = new TblPoolquestionoption(answer,5,0,tblPoolquestion);
					poolQuestionOptionRepository.save(tblPoolquestionoption);
				}
				
				
			}
		}
			
		
	}

	@Override
	/*	public void savePool(String[] arr, String poolName, String poolDesc) {
	*/		public void savetoExistingPool(String[] arr, String poolName, String loginName) {


	/*		TblAssignment tblassignment = assignmentRepository.findTblAssignmentByassignID(400005); 
	*/		
			
		/*	TblPool tblPool = new TblPool(poolName,poolDesc );
			TblPool tblPool = new TblPool(poolName, loginName);
			poolRepository.save(tblPool);*/
	          int poolID = poolRepository.findPoolID(poolName, loginName);
			for(int i = 0; i < (arr.length/5)*5; i = i+5){
				
				TblPoolquestion tblPoolquestion = new TblPoolquestion(3, arr[i], poolRepository.findTblPooltBypoolID(poolID));
				poolQuestionRepository.save(tblPoolquestion);
				
				for(int j=i+1; j<i+5; j++){
						String answer = arr[j].toLowerCase();
					
					if(answer.contains("correct answer")){
						answer = answer.replace("correct answer", "");
						//String choice, int displayOrder, int fractionCorrect,TblPoolquestion tblPoolquestion
						TblPoolquestionoption tblPoolquestionoption = new TblPoolquestionoption(answer,5,1,tblPoolquestion);
						poolQuestionOptionRepository.save(tblPoolquestionoption);
					}else{
						TblPoolquestionoption tblPoolquestionoption = new TblPoolquestionoption(answer,5,0,tblPoolquestion);
						poolQuestionOptionRepository.save(tblPoolquestionoption);
					}
					
					
				}
			}
				
			
		}
	// COMMENTED OUT THIS METHOD AS IT IS NOT USED 
	/*public int  findpoolIDByassignID(int assignID) {
		// TODO Auto-generated method stub
		return poolRepository.findpoolIDByassignID(assignID);
	}*/

	public List<TblPool> getPools(String loginName) {
		// TODO Auto-generated method stub
		
		return  poolRepository.findunassignedPools(loginName);
	}

	
	
	
	
	

	@Override
	public TblPool findTblPoolByassignID(int assignID) {
		// TODO Auto-generated method stub
		return poolRepository.findTblPoolByassignID(assignID);
	}

	@Override
	public void saveQuiz(TblQuiz tblQuiz) {
		// TODO Auto-generated method stub
		quizRepository.save(tblQuiz);
		
	}

	public List<TblPool> findPools() {
		// TODO Auto-generated method stub
		return poolRepository.findAll();
	}

	public void removePool(int poolID) {
		
		// TODO Auto-generated method stub
		TblPool tblPool = poolRepository.findTblPooltBypoolID(poolID);
		
		List<TblPoolquestion> questionList = tblPool.getTblPoolquestions();
		
		
		
		for (int i = 0; i < questionList.size(); i++) {
						
						
				List<TblPoolquestionoption> optionList = questionList.get(i).getTblPoolquestionoptions();
				
					
				for (int j = 0; j < optionList.size(); j++) {
					
					poolQuestionOptionRepository.delete(optionList.get(j).getPoolQuestionOptionID());
					
				
				}
							poolQuestionRepository.delete(questionList.get(i).getPoolQuestionID());
				
			
		}
		/*System.out.println("@@@@@@@@ "+ questionList.get(0));
		System.out.println("@@@@@@@@ "+ questionList.get(0).getTblPoolquestionoptions());*/

		poolRepository.delete(poolID);
	
		}
	
	
	
	

	// create Pool record while creating an assignment
	public void createPoolRecord(String poolName,TblAssignment assignment) {
		// TODO Auto-generated method stub
		poolRepository.save(new TblPool(poolName,assignment));
	}

	@Override
	public void createPool(String poolName, String poolDesc) {
		// TODO Auto-generated method stub
				
		poolRepository.save(new TblPool(poolName,poolDesc ));
		
		
	}

	
	public void updatePool(String jsonstring) throws JsonProcessingException, IOException {		
		
		ObjectMapper mapper = new ObjectMapper();		
		JsonNode root = mapper.readTree(jsonstring);
		
		/*JsonNode contactNode1 = root.path("ids");	
		JsonNode contactNode2 = root.path("names");
		JsonNode contactNode3 = root.path("descs");*/
		
		for (int i=0; i<root.path("ids").size(); i++) {
			
			TblPool tblPool = poolRepository.findOne(root.path("ids").get(i).asInt());
			String oldPoolName =  tblPool.getPoolName();
/*			String oldPoolDesc =  tblPool.getPoolDesc();
*/			
			
			//int node1 = root.path("ids").get(i).asInt();
/*			tblPool.setPoolDesc(root.path("descs").get(i).asText());
*/			tblPool.setPoolName(root.path("names").get(i).asText());
			poolRepository.save(tblPool);
			
     List<TblPool> tblPools = poolRepository.findTblPoolIDBypoolName(oldPoolName) ;
			
			for (TblPool tblPool1: tblPools) {
/*				tblPool1.setPoolDesc(root.path("descs").get(i).asText());
*/				tblPool1.setPoolName(root.path("names").get(i).asText());
				
				poolRepository.save(tblPool1);
			}
		}
	}

	public List<TblPoolquestionoption> getChoices(Integer questionID) {
		// TODO Auto-generated method stub
		return poolQuestionOptionRepository.getChoices(questionID);
	}

	@Override
	public List<TblPoolquestion> getQuestions(Integer poolID) {
		// TODO Auto-generated method stub
		return poolQuestionRepository.getQuestions(poolID);
	}

	public void updateQuestionOptions(String totalQuestionOptions, Integer poolID) throws JsonProcessingException, IOException {
		
		ObjectMapper mapper = new ObjectMapper();		
		JsonNode root = mapper.readTree(totalQuestionOptions);
		
		

		for (int i=0; i<root.path("poolQuestionIDs").size(); i++) {

			TblPoolquestion tblPoolquestion = poolQuestionRepository.findOne(root.path("poolQuestionIDs").get(i).asInt());
			
			tblPoolquestion.setQuestion(root.path("questions").get(i).asText());
			
			List<TblPoolquestionoption> optionsList = poolQuestionOptionRepository.getChoices(root.path("poolQuestionIDs").get(i).asInt());
			int count = 0;			
			for(int k=0; k<4; k++) {
				count++;
				optionsList.get(k).setChoice(root.path("options").get(4*i + k).asText());
				if(count == root.path("correctChoices").get(i).asInt()){
					optionsList.get(k).setFractionCorrect(1);
				}else{
					optionsList.get(k).setFractionCorrect(0);
				}
			}
			poolQuestionRepository.save(tblPoolquestion);
			
			for (TblPoolquestionoption t : optionsList)
			{
			poolQuestionOptionRepository.save(t);
			}
		   
		}
	}

	public void removeQuestions(Integer poolQuestionID) {
            
					
		List<TblPoolquestionoption> optionsList = poolQuestionOptionRepository.getChoices(poolQuestionID);
		
		for (TblPoolquestionoption t : optionsList)
		{
		poolQuestionOptionRepository.delete(t.getPoolQuestionOptionID());
		}

		poolQuestionRepository.delete(poolQuestionID);
		
		
	}

	public void savenewQuestionOptions(String newQuestionOptions, Integer poolID) throws JsonProcessingException, IOException {
		
		ObjectMapper mapper = new ObjectMapper();		
		JsonNode root = mapper.readTree(newQuestionOptions);
		
		TblPool tblPool = poolRepository.findOne(poolID);
		for (int i=0; i<root.path("questions").size(); i++) {
            			
			TblPoolquestion tblPoolquestion = new TblPoolquestion(0,root.path("questions").get(i).asText(),tblPool);
			poolQuestionRepository.save(tblPoolquestion);
			int count =0;
			for (int j= 4*i; j<4*i + 4; j++) {
				count++;
				if(count == root.path("correctChoices").get(i).asInt()){
					TblPoolquestionoption tblPoolquestionoption = new TblPoolquestionoption(root.path("options").get(j).asText(),1, 1, tblPoolquestion);
					poolQuestionOptionRepository.save(tblPoolquestionoption);
					}else{
						TblPoolquestionoption tblPoolquestionoption = new TblPoolquestionoption(root.path("options").get(j).asText(),1, 0, tblPoolquestion);
						poolQuestionOptionRepository.save(tblPoolquestionoption);				}
				// String choice, int displayOrder, int fractionCorrect,
				//TblPoolquestion tblPoolquestio
								
			}
		}
	}

	public List<TblStudentquiz> findAllTblStudentquiz() {
		// TODO Auto-generated method stub
		return studentquizRepository.findAll();
	}

	public TblPool findPoolIDBypoolName(String poolName) {
		// TODO Auto-generated method stub
		return poolRepository.findPoolIDBypoolName(poolName);
	}

	public void saveStudentQuizAcess(String json) throws JsonProcessingException, IOException {
		// TODO Auto-generated method stub
		
		ObjectMapper mapper = new ObjectMapper();		
		JsonNode root = mapper.readTree(json);
		
		
		for (int i=0; i<root.path("studentIDs").size(); i++) {
			
			int studentID = root.path("studentIDs").get(i).asInt();
			int quizID = root.path("quizIDs").get(i).asInt();

            
			TblStudentquiz tblStudentquiz = studentquizRepository.findTblStudentquizbystuIDnquizID(studentID, quizID);
			tblStudentquiz.setGrade("pass");
			studentquizRepository.save(tblStudentquiz);
		
	}

	}

	/*public String findpoolNameBypoolDesc(String poolDesc) {
		// TODO Auto-generated method stub
		return poolRepository.findpoolNameBypoolDesc(poolDesc);
	}*/

	public boolean checkPoolName(String poolName, String loginName) {
		// TODO Auto-generated method stub
		
		List<TblPool> tblPoolList = poolRepository.checkPoolNameAndSave(poolName, loginName);
		
		if (tblPoolList.size() >= 1){
			
			return false ;
		} else {
			
			return true;
		}
	}

	public boolean checkPoolNameAndSave(Integer poolID, String poolName, String loginName) {
		// TODO Auto-generated method stub
		
		List<TblPool> tblPoolList = poolRepository.checkPoolNameAndSave(poolName, loginName);
		if (tblPoolList.size() >= 1){
			return false ;
		} else
		{
			
			TblPool tblPool = poolRepository.findTblPooltBypoolID(poolID);
			tblPool.setPoolName(poolName);
			poolRepository.save(tblPool);
			return true;
		}
		
	}
}
