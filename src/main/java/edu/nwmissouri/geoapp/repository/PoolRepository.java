package edu.nwmissouri.geoapp.repository;

//import org.hibernate.mapping.List;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.nwmissouri.geoapp.model.TblPool;
import edu.nwmissouri.geoapp.model.TblPoolquestion;
import edu.nwmissouri.geoapp.model.TblQuiz;


public interface PoolRepository extends JpaRepository<TblPool, Integer> {

	
	public TblPool findTblPooltBypoolID(int poolID);
	
	@Query("select poolID from TblPool a where a.poolName =?1 and a.poolCreatedBy =?2 ")
	public int findPoolID(String poolName, String loginName);

	/*@Query("select poolID from TblPool a where a.tblAssignment.assignID =?1")
    public int findpoolIDByassignID(int assignID);*/
	
	@Query("select a from TblPool a where a.tblAssignment.assignID =?1")
    public TblPool findTblPoolByassignID(int assignID);
	
	@Query("select a from TblPool a where a.poolCreatedBy=?1 and a.tblAssignment.assignID is NULL")
	public List<TblPool> findunassignedPools(String loginName);
	
	
	@Query("select a from TblPool a where a.poolName =?1 and a.tblAssignment.assignID is NULL")
	public TblPool findPoolIDBypoolName(String poolName);
	
	@Query("select a from TblPool a where a.poolName =?1 and a.tblAssignment.assignID is NOT NULL")
	public List<TblPool> findTblPoolIDBypoolName(String poolName);
	
	/*@Query("select poolName from TblPool a where a.poolDesc =?1 and a.tblAssignment.assignID is NULL")
	public String findpoolNameBypoolDesc(String poolDesc);*/
	
	@Query("select a from TblPool a where a.poolName =?1 and a.tblAssignment.assignID is NULL")
	public List<TblPool> checkPoolName(String poolName);
	
	@Query("select a from TblPool a where a.poolName =?1 and a.poolCreatedBy=?2 and a.tblAssignment.assignID is NULL")
	public List<TblPool> checkPoolNameAndSave(String poolName, String loginName);

}
