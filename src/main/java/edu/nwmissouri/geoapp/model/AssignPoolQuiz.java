package edu.nwmissouri.geoapp.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the tbl_assignment database table.
 * 
 */

public class AssignPoolQuiz {
	
	private int assignID;
	
	private String name;
	
	private BigDecimal possiblepointsphase1;

	private BigDecimal possiblepointsphase2;

	private BigDecimal possiblepointsphase3;
	
	private String poolName;
	
	private String quizName;
	
	private int numQuestions;
	
	private int num_Takes_Max;
	
	private int qualpercent; 
	
	private int lockoutHrs;

	private String timer;
	
	private String isActive;
	
	@Temporal(TemporalType.DATE)
	private Date due_date;

	private String description;

	// Getter and Setters 

	
	public String getName() {
		return name;
	}

	public int getAssignID() {
		return assignID;
	}

	public void setAssignID(int assignID) {
		this.assignID = assignID;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPossiblepointsphase1() {
		return possiblepointsphase1;
	}

	public void setPossiblepointsphase1(BigDecimal possiblepointsphase1) {
		this.possiblepointsphase1 = possiblepointsphase1;
	}

	public BigDecimal getPossiblepointsphase2() {
		return possiblepointsphase2;
	}

	public void setPossiblepointsphase2(BigDecimal possiblepointsphase2) {
		this.possiblepointsphase2 = possiblepointsphase2;
	}

	public BigDecimal getPossiblepointsphase3() {
		return possiblepointsphase3;
	}

	public void setPossiblepointsphase3(BigDecimal possiblepointsphase3) {
		this.possiblepointsphase3 = possiblepointsphase3;
	}

	public String getPoolName() {
		return poolName;
	}

	public void setPoolName(String poolName) {
		this.poolName = poolName;
	}

	public String getQuizName() {
		return quizName;
	}

	public void setQuizName(String quizName) {
		this.quizName = quizName;
	}

	public int getNumQuestions() {
		return numQuestions;
	}

	public void setNumQuestions(int numQuestions) {
		this.numQuestions = numQuestions;
	}

	public int getNum_Takes_Max() {
		return num_Takes_Max;
	}

	public void setNum_Takes_Max(int num_Takes_Max) {
		this.num_Takes_Max = num_Takes_Max;
	}

	public int getQualpercent() {
		return qualpercent;
	}

	public void setQualpercent(int qualpercent) {
		this.qualpercent = qualpercent;
	}

	public int getLockoutHrs() {
		return lockoutHrs;
	}

	public void setLockoutHrs(int lockoutHrs) {
		this.lockoutHrs = lockoutHrs;
	}

	public String getTimer() {
		return timer;
	}

	public void setTimer(String timer) {
		this.timer = timer;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public Date getDue_date() {
		return due_date;
	}

	public void setDue_date(Date due_date) {
		this.due_date = due_date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public AssignPoolQuiz() {
	}

	

	public AssignPoolQuiz(int assignID, String name, BigDecimal possiblepointsphase1, BigDecimal possiblepointsphase2,
			BigDecimal possiblepointsphase3, String quizName, int numQuestions, int num_Takes_Max,
			int qualpercent, int lockoutHrs, String timer, String isActive, Date due_date, String description, String poolName) {
		super();
		this.assignID = assignID;
		this.name = name;
		this.possiblepointsphase1 = possiblepointsphase1;
		this.possiblepointsphase2 = possiblepointsphase2;
		this.possiblepointsphase3 = possiblepointsphase3;
/*		this.poolDesc = poolDesc;
*/		this.quizName = quizName;
		this.numQuestions = numQuestions;
		this.num_Takes_Max = num_Takes_Max;
		this.qualpercent = qualpercent;
		this.lockoutHrs = lockoutHrs;
		this.timer = timer;
		this.isActive = isActive;
		this.due_date = due_date;
		this.description = description;
		this.poolName = poolName;
	}


}